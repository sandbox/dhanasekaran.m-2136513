CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Permissions
 * Usage

INTRODUCTION
------------

Facebook Block is a lightweight module which allows administrators to create
'n' number of blocks which display embedded timelines.

Facebook Block will never need App Id, App secret to configure.

REQUIREMENTS
------------

Facebook Block has one dependency.

Drupal core modules
 * Block

INSTALLATION
------------

Facebook Block can be installed via the standard Drupal installation process
(http://drupal.org/documentation/install/modules-themes/modules-7).

PERMISSIONS
------------

The ability to create, edit and delete Facebook Block relies on the block
module's "Administer blocks" permission.

USAGE
-----

Administrators can visit the Blocks administration page where they can create
new Facebook Block and update or delete existing Facebook Block.

We can Place many number of facebook blocks in a site.

Administrators can also position Facebook Block as they can with standard
or custom blocks provided by the core Block module.

Each Facebook Block block requires a unique Facebook Page which determines, among
other things, the source of the
feeds to display.

Refer https://www.facebook.com/pages/create/ on how to create a Facebook Page.
