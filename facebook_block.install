<?php

/**
 * @file
 * Install, update and uninstall functions for the facebook_block module.
 */

/**
 * Implements hook_schema().
 */
function facebook_block_schema() {
  $schema['facebook_block'] = array(
    'description' => 'The table for storing facebook blocks.',
    'fields' => array(
      'bid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => "The block's {block}.bid.",
      ),
      'info' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Block description.',
      ),
      'facebook_page' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Facebook Page.',
      ),
      'data' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE,
        'description' => 'Serialized data containing the timeline properties.',
      ),
    ),
    'unique keys' => array(
      'info' => array('info'),
      'facebook_page' => array('facebook_page'),
    ),
    'primary key' => array('bid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function facebook_block_uninstall() {
  // Remove blocks
  db_delete('block')
    ->condition('module', 'facebook_block')
    ->execute();
  db_delete('block_role')
    ->condition('module', 'facebook_block')
    ->execute();

  // Clear the site cache
  cache_clear_all();
}

/**
 * Remove any old Facebook Block blocks and install the new Facebook Block schema.
 */
function facebook_block_update_7200() {
  // Remove old Facebook Block schema
  drupal_uninstall_schema('facebook_block');

  // Remove any old Facebook Block blocks
  db_delete('block')
    ->condition('module', 'facebook_block')
    ->execute();
  db_delete('block_role')
    ->condition('module', 'facebook_block')
    ->execute();

  // Clear the site cache
  cache_clear_all();

  // Install the new Facebook Block schema
  drupal_install_schema('facebook_block');

  return t('Removed any old Facebook Block blocks and installed the new Facebook Block schema.');
}
