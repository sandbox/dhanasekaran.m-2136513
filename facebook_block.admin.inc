<?php

/**
 * @file
 * Admin page callbacks for the Facebook Block module.
 */

/**
 * Form constructor for the add block form.
 *
 * @see facebook_block_add_block_form_validate()
 * @see facebook_block_add_block_form_submit()
 * @ingroup forms
 */
function facebook_block_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  $form = block_admin_configure($form, $form_state, 'facebook_block', NULL);

  // Other modules should be able to use hook_form_block_add_block_form_alter()
  // to modify this form, so add a base form ID
  $form_state['build_info']['base_form_id'] = 'block_add_block_form';

  // Prevent block_add_block_form_validate/submit() from being automatically
  // added because of the base form ID by providing these handlers manually
  $form['#validate'] = array('facebook_block_add_block_form_validate');
  $form['#submit'] = array('facebook_block_add_block_form_submit');

  return $form;
}

/**
 * Form validation handler for facebook_block_add_block_form().
 *
 * @see facebook_block_add_block_form()
 * @see facebook_block_add_block_form_submit()
 */
function facebook_block_add_block_form_validate($form, &$form_state) {
  $unique_description = (bool) db_query_range('SELECT 1 FROM {facebook_block} WHERE info = :info', 0, 1, array(':info' => $form_state['values']['info']))->fetchField();

  if (empty($form_state['values']['info']) || $unique_description) {
    form_set_error('info', t('Ensure that each block description is unique.'));
  }

  $unique_facebook_page = (bool) db_query_range('SELECT 1 FROM {facebook_block} WHERE facebook_page = :facebook_page', 0, 1, array(':facebook_page' => $form_state['values']['facebook_page']))->fetchField();

  if (empty($form_state['values']['facebook_page']) || $unique_facebook_page) {
    form_set_error('facebook_page', t('Ensure that each block Facebook Page is unique.'));
  }
}

/**
 * Form submission handler for facebook_block_add_block_form().
 *
 * Saves the new custom Facebook block.
 *
 * @see facebook_block_add_block_form()
 * @see facebook_block_add_block_form_validate()
 */
function facebook_block_add_block_form_submit($form, &$form_state) {
  // The serialized 'data' column contains the timeline settings.
  
  $data = array(
    'fb_likebox_header' => $form_state['values']['fb_likebox_header'],
    'fb_likebox_stream' => $form_state['values']['fb_likebox_stream'],
    'fb_likebox_show_faces' => $form_state['values']['fb_likebox_show_faces'],
    'fb_likebox_scrolling' => $form_state['values']['fb_likebox_scrolling'],
    'fb_likebox_force_wall' => $form_state['values']['fb_likebox_force_wall'],
    'fb_likebox_show_border' => $form_state['values']['fb_likebox_show_border'],
    'fb_likebox_width' => $form_state['values']['fb_likebox_width'],
    'fb_likebox_height' => $form_state['values']['fb_likebox_height'],
    'fb_likebox_colorscheme' => $form_state['values']['fb_likebox_colorscheme'],
  );

  // Save the block configuration
  $delta = db_insert('facebook_block')
    ->fields(array(
      'info' => $form_state['values']['info'],
      'facebook_page' => $form_state['values']['facebook_page'],
      'data' => serialize($data),
    ))
    ->execute();

  // Store block delta to allow other modules to work with new block
  $form_state['values']['delta'] = $delta;

  // Run the normal new block submission (borrowed from block_add_block_form_submit)
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'status' => 0,
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  // Store regions per theme for this block
  foreach ($form_state['values']['regions'] as $theme => $region) {
    db_merge('block')
      ->key(array('theme' => $theme, 'delta' => $delta, 'module' => $form_state['values']['module']))
      ->fields(array(
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'pages' => trim($form_state['values']['pages']),
        'status' => (int) ($region != BLOCK_REGION_NONE),
      ))
      ->execute();
  }

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Form constructor for the custom Facebook block deletion form.
 *
 * @param $delta
 *   The unique ID of the block within the context of $module.
 *
 * @see facebook_block_delete_submit()
 */
function facebook_block_delete($form, &$form_state, $delta) {
  $block = block_load('facebook_block', $delta);
  $facebook_block = facebook_block_block_get($block->delta);
  $form['info'] = array('#type' => 'hidden', '#value' => $facebook_block['info'] ? $facebook_block['info'] : $facebook_block['title']);
  $form['bid'] = array('#type' => 'hidden', '#value' => $block->delta);

  return confirm_form($form, t('Are you sure you want to delete the block %name?', array('%name' => $facebook_block['info'])), 'admin/structure/block', '', t('Delete'), t('Cancel'));
}

/**
 * Form submission handler for facebook_block_delete().
 *
 * @see facebook_block_delete()
 */
function facebook_block_delete_submit($form, &$form_state) {
  db_delete('facebook_block')
    ->condition('bid', $form_state['values']['bid'])
    ->execute();
  db_delete('block')
    ->condition('module', 'facebook_block')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  db_delete('block_role')
    ->condition('module', 'facebook_block')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  drupal_set_message(t('The block %name has been removed.', array('%name' => $form_state['values']['info'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}
